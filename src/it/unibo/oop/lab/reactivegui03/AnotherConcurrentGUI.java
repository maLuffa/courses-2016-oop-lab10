package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnotherConcurrentGUI extends JFrame {
    /**
     * 
     */
    private static final long serialVersionUID = -8630968055862320453L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final Agent agent = new Agent();
    
    /**
     * Builds a new CGui.
     */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        /*
         * Create the counter agent and start it. This is actually not so good:
         * thread management should be left to
         * java.util.concurrent.ExecutorService
         */
        //agent.start();
        new Thread(() -> {
            try {
                agent.start();
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            this.stopAction();
        }).start();
        /*
         * Register a listener that stops it
         */
        up.addActionListener(e -> agent.goingUp());
        down.addActionListener(e -> agent.goingDown());
        stop.addActionListener(e -> this.stopAction());    
    }
    private void stopAction() {
        this.agent.stopCounting();
        this.up.setEnabled(false);
        this.down.setEnabled(false);
    }
    /*
     * The counter agent is implemented as a nested class. This makes it
     * invisible outside and incapsulated.
     */
    private class Agent extends Thread {
        private int counter;
        private volatile boolean stop;
        private volatile boolean up;
        /**
         * Initialazing the counter to 0 and the up field to true
         */
       Agent() {
            this.counter = 0;
            this.up = true;
        }
        public void stopCounting() {
            this.stop = true;
        }
        public void goingDown() {
            this.up = false;
        }
        public void goingUp() {
            this.up = true;
        }
        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if (this.up) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }
    }
}
